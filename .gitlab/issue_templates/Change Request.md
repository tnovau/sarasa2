# Change Request Title

## Objetivo

Modifica #[N° de PBI].

Como un [actor] quiero poder [acción] para entonces [logro].

****

## Criterios de aceptación

- Criterio de aceptación 1.
- Criterio de aceptación 2.

## Descripción del cambio solicitado

## Documentación

[Vínculo a documentación](http://link)

/label ~pbi ~cr